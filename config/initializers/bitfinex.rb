Bitfinex::Client.configure do |conf|
  conf.secret  = RbTradebot.config[:bfx_api_secret]
  conf.api_key = RbTradebot.config[:bfx_api_key]

  # uncomment if you want to use version 2 of the API
  # which is opt-in at the moment
  #
  # conf.use_api_v2
end
