require 'csv'

files = Dir.glob("#{Rails.root}/app/data/*/*")

# Timestamp	          Open      High      Low	  Close	    Volume (BTC)  Volume (Currency)	Weighted Price
# 2017-12-13 00:00:00	16650.01	16675.77	16650 16675.77  10.46	        174229.4	        16652.85
# 2017-12-13 00:01:00	16680	    16700	    16650 16650.01  33.14	        553098.01	        16689.47

DATA = files.each_with_object({}) do |f, hsh|
  pair, date = f.gsub('.tsv', '').split('/')[-2..-1]

  hsh[pair] ||= {}
  hsh[pair][date] = CSV.read(f, col_sep: "\t")
end
