Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :statistics, only: %i[] do
    collection do
      get :orders_by_time
    end
  end

  resources :balances, only: %i[index update] do
    collection do
      patch :reset
    end
  end

  resources :orders, only: %i[index] do
    collection do
      patch :destroy_all
    end
  end

  resources :bots, only: %i[] do
    collection do
      patch :start
    end
  end
end
