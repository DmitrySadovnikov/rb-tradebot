# frozen_string_literal: true

require 'rails_helper'

describe Bot::Start do
  let(:client) { Client.create! }

  context 'when success' do
    context 'when real data' do
      subject { described_class.run!(date: date) }

      let(:date) { '2017-12-13' }
      let(:data) { DATA.dig('usdbtc', date) }
      let!(:balances) do
        Balance.create! [{ client:    client,
                           'currency' => 'usd',
                           'amount'   => '1.00940209338' },
                         { client:    client,
                           'currency' => 'btc',
                           'amount'   => '0.02628206' }]
      end

      it do
        expect(subject).to eq :ok
      end

      it 'parsed time in utc' do
        time = Time.zone.parse(data.first.first)
        expect(time).to eq time.beginning_of_day
      end
    end
  end
end
