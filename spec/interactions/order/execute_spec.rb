# frozen_string_literal: true

require 'rails_helper'

describe Order::Execute do
  let(:client)       { Client.create! }
  let!(:balance_usd) { create :balance, :usd, client: client, amount: 10_000 }
  let!(:balance_btc) { create :balance, :btc, client: client, amount: 1.2 }

  subject do
    described_class.run! order:          order,
                         current_amount: 0.5,
                         executed_at:    Time.current
  end

  context 'when buy btc for usd' do
    let(:order) do
      create :order, :buy,
             client:          client,
             price:           5_000,
             original_amount: 0.5
    end

    it do
      subject

      expect(balance_usd.reload.amount).to     eq 5000.to_d
      expect(balance_usd.reload.available).to  eq 5000.0.to_d
      expect(balance_btc.reload.amount).to     eq 1.7.to_d
      expect(balance_btc.reload.available).to  eq 1.7.to_d
      expect(order.reload.executed_amount).to  eq 0.5.to_d
      expect(order.reload.remaining_amount).to eq 0.0.to_d
      expect(order.reload.status).to           eq 'executed'
    end
  end

  context 'when sell btc for usd' do
    let(:order) do
      create :order, :sell,
             client:          client,
             price:           10_000,
             original_amount: 0.5
    end

    it do
      subject

      expect(balance_usd.reload.amount).to     eq 20_000.to_d
      expect(balance_usd.reload.available).to  eq 20_000.to_d
      expect(balance_btc.reload.amount).to     eq 0.7.to_d
      expect(balance_btc.reload.available).to  eq 0.7.to_d
      expect(order.reload.executed_amount).to  eq 0.5.to_d
      expect(order.reload.remaining_amount).to eq 0.0.to_d
      expect(order.reload.status).to           eq 'executed'
    end
  end
end
