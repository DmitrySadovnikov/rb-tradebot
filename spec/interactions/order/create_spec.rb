# frozen_string_literal: true

require 'rails_helper'

describe Order::Create do
  let(:client) { Client.create! }
  let!(:balance_usd) { create :balance, :usd, client: client, amount: 10_000 }
  let!(:balance_btc) { create :balance, :btc, client: client, amount: 1.2 }

  subject { described_class.run!(params) }

  context 'when success' do
    context 'when buy btc for usd' do
      let(:params) do
        {
          symbol:     'usdbtc',
          amount:     1.0,
          side:       'buy',
          price:      10_000,
          client:     client,
          timestamp:  Time.current.to_i,
          created_at: Time.current,
          unit_price: 20_000
        }
      end

      it do
        order = subject

        expect(balance_usd.reload.amount).to     eq 10_000.0.to_d
        expect(balance_usd.reload.available).to  eq 0.0.to_d
        expect(balance_btc.reload.amount).to     eq 1.2.to_d
        expect(balance_btc.reload.available).to  eq 1.2.to_d
        expect(order.reload.original_amount).to  eq 1.0.to_d
        expect(order.reload.executed_amount).to  eq 0.0.to_d
        expect(order.reload.remaining_amount).to eq 1.0.to_d
      end
    end

    context 'when sell btc for usd' do
      let(:params) do
        {
          symbol:     'usdbtc',
          amount:     1,
          side:       'sell',
          price:      20_000,
          client:     client,
          timestamp:  Time.current.to_i,
          created_at: Time.current,
          unit_price: 20_000
        }
      end
      it do
        order = subject

        expect(balance_usd.reload.amount).to     eq 10_000.0.to_d
        expect(balance_usd.reload.available).to  eq 10_000.0.to_d
        expect(balance_btc.reload.amount).to     eq 1.2.to_d
        expect(balance_btc.reload.available).to  eq 0.2.to_d
        expect(order.reload.original_amount).to  eq 1.0.to_d
        expect(order.reload.executed_amount).to  eq 0.0.to_d
        expect(order.reload.remaining_amount).to eq 1.0.to_d
      end
    end
  end
end
