FactoryGirl.define do
  factory :balance do
    client { Client.create! }

    trait :usd do
      amount   { 1040.50 }
      currency { 'usd' }
    end

    trait :btc do
      amount   { 1 }
      currency { 'btc' }
    end
  end
end
