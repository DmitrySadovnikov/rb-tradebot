FactoryGirl.define do
  factory :order do
    client      { Client.create! }
    symbol      { 'usdbtc' }
    timestamp   { Time.current.to_i }
    created_at  { Time.current }
    executed_at nil

    trait :sell do
      side            { 'sell' }
      price           { 19_000.0 }
      original_amount { 1 }
      executed_amount { 0 }
    end

    trait :buy do
      side            { 'buy' }
      price           { 13_000.0 }
      original_amount { 1 }
      executed_amount { 0 }
    end
  end
end
