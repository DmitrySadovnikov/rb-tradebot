# frozen_string_literal: true

class DataPresenter
  def initialize(data)
    @data = data
  end

  def where(**params)
    params.each do |k, v|
      @data = @data.select do |row|
        row[k.to_s] == v
      end
    end

    @data
  end

  def find_by(**params)
    where(**params).first
  end

  private

  def method_missing(method)
    @data[method]
  end
end
