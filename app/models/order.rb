# frozen_string_literal: true

class Order < ApplicationRecord

  # [{"id"=>6243513529,
  #   "cid"=>73140763864,
  #   "cid_date"=>"2017-12-17",
  #   "gid"=>nil,
  #   "symbol"=>"btcusd",
  #   "exchange"=>nil,
  #   "price"=>"100000.0",
  #   "avg_execution_price"=>"0.0",
  #   "side"=>"sell",
  #   "type"=>"exchange limit",
  #   "timestamp"=>"1513541941.0",
  #   "is_live"=>true,
  #   "is_cancelled"=>false,
  #   "is_hidden"=>false,
  #   "oco_order"=>nil,
  #   "was_forced"=>false,
  #   "original_amount"=>"0.01",
  #   "remaining_amount"=>"0.01",
  #   "executed_amount"=>"0.0",
  #   "src"=>"web"},
  #  {"id"=>6243569613,
  #   "cid"=>73308351469,
  #   "cid_date"=>"2017-12-17",
  #   "gid"=>nil,
  #   "symbol"=>"btcusd",
  #   "exchange"=>nil,
  #   "price"=>"4.0",
  #   "avg_execution_price"=>"0.0",
  #   "side"=>"buy",
  #   "type"=>"exchange limit",
  #   "timestamp"=>"1513542108.0",
  #   "is_live"=>true,
  #   "is_cancelled"=>false,
  #   "is_hidden"=>false,
  #   "oco_order"=>nil,
  #   "was_forced"=>false,
  #   "original_amount"=>"1.0",
  #   "remaining_amount"=>"1.0",
  #   "executed_amount"=>"0.0",
  #   "src"=>"web"}]
  MARKET_CURRENCY = 'usd'
  TOKEN_CURRENCY  = 'btc'
  MAKER_FEE_RATE  = 0.001
  TAKER_FEE_RATE  = 0.002

  belongs_to :client

  # scope :active, -> { where('original_amount - executed_amount != 0.0') }
  # scope :executed, -> { where('original_amount - executed_amount = 0.0') }

  enum status: {
    active:    0,
    cancelled: 1,
    executed:  2
  }

  validates :client,
            :symbol,
            :timestamp,
            :side,
            :price,
            :original_amount,
            :executed_amount,
            presence: true

  validates :original_amount,
            :executed_amount,
            :price,
            numericality: { greater_than_or_equal_to: 0 }

  validate :check_original_amount, on: :create

  def check_original_amount
    return if original_amount <= client_balance.amount
    raise 'invalid original_amount'
  end

  def client_balance
    client.balances.find_by(currency: holt_currency)
  end

  def remaining_amount
    original_amount - executed_amount
  end

  def execute(amount, executed_at: nil)
    Order::Execute.run!(order:          self,
                        current_amount: amount,
                        executed_at:    executed_at)
  end

  def self.index
    Order::Index.run!
  end

  def holt_amount
    case side
    when 'buy'  then price
    when 'sell' then remaining_amount
    end
  end

  def holt_currency
    case side
    when 'buy'  then MARKET_CURRENCY
    when 'sell' then TOKEN_CURRENCY
    end
  end
end
