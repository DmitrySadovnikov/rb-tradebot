# frozen_string_literal: true

class Balance < ApplicationRecord
  belongs_to :client

  validates :amount, :currency, presence: true
  validates :amount, numericality: { greater_than_or_equal_to: 0 }

  def available
    amount - client.orders.active.sum { |o| o.holt_currency == currency ? o.holt_amount : 0.0 }
  end

  class << self
    def index
      Balance::Index.run!
    end

    def reset
      Balance.destroy_all
      client = Client.first

      Balance.create!(client:   client,
                      currency: 'btc',
                      amount:   0.0)

      Balance.create!(client:   client,
                      currency: 'usd',
                      amount:   100)
    end
  end
end
