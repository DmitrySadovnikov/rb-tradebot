# frozen_string_literal: true

class Client < ApplicationRecord
  has_many :orders
  has_many :balances

  def new_order(symbol, amount, _type, side, price, timestamp: nil, created_at: nil, unit_price: nil)
    Order::Create.run!(
      symbol:     symbol,
      amount:     amount,
      side:       side,
      price:      price,
      client:     self,
      timestamp:  timestamp,
      created_at: created_at,
      unit_price: unit_price
    )
  end

  def cancel_orders(ids=nil, cancelled_at:)
    case ids
    when Array
      orders.where(id: ids).update_all(status: :cancelled, cancelled_at: cancelled_at)
    when Numeric, String
      orders.find(ids).update!(status: :cancelled, cancelled_at: cancelled_at)
    when NilClass
      orders.update_all(status: :cancelled, cancelled_at: cancelled_at)
    else
      raise ParamsError
    end
  end
end
