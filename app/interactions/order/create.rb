# frozen_string_literal: true

class Order < ApplicationRecord
  class Create < ActiveInteraction::Base
    private

    string  :symbol
    decimal :amount
    string  :side
    decimal :price
    object  :client, class: Client
    integer :timestamp
    time    :created_at
    decimal :unit_price

    def execute
      case side
      when 'buy'  then check_money_balance
      when 'sell' then check_token_balance
      else             raise 'wtf?'
      end

      Order.create!(
        client:          client,
        symbol:          symbol,
        original_amount: amount.round(8),
        executed_amount: 0.0,
        timestamp:       timestamp,
        side:            side,
        price:           price.round(8),
        created_at:      created_at,
        unit_price:      unit_price
      )
    end

    def money_balance
      @_money_balance ||=
        client.balances.find_by(currency: Order::MARKET_CURRENCY)
    end

    def token_balance
      @_token_balance ||=
        client.balances.find_by(currency: Order::TOKEN_CURRENCY)
    end

    # validate

    def check_money_balance
      return if money_balance.available >= (price * amount).round(8)
      raise 'not enough money'
    end

    def check_token_balance
      return if token_balance.available >= amount
      raise 'not enough tokens'
    end
  end
end
