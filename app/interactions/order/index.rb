# frozen_string_literal: true

class Order < ApplicationRecord
  class Index < ActiveInteraction::Base
    private

    date :date, default: nil

    def execute
      orders.order(:id).map do |order|
        order.as_json.tap do |hsh|
          hsh['remaining_amount'] = order.remaining_amount
          hsh['created_at']       = prepare_time(order.created_at)
          hsh['executed_at']      = prepare_time(order.executed_at)
          hsh['cancelled_at']     = prepare_time(order.cancelled_at)
        end
      end.as_json
    end

    def orders
      @_orders ||= begin
        if date
          Order.where(created_at: date.all_day)
        else
          Order.all
        end
      end
    end

    def prepare_time(time)
      return unless time
      time.strftime('%H:%M %d.%m.%Y')
    end
  end
end
