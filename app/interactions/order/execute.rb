# frozen_string_literal: true

class Order < ApplicationRecord
  class Execute < ActiveInteraction::Base
    private

    object  :order
    decimal :current_amount
    time    :executed_at

    def execute
      case order.side
      when 'buy'
        token_balance.amount += current_amount
        money_balance.amount -= order.price
      when 'sell'
        money_balance.amount += order.price
        token_balance.amount -= current_amount
      end

      order.executed_amount += current_amount
      order.executed_at = executed_at

      raise 'fuck off' if order.remaining_amount.negative?

      ActiveRecord::Base.transaction do
        order.save!
        order.executed! if order.remaining_amount == 0.0
        money_balance.save!
        token_balance.save!
      end
    end

    def money_balance
      @_money_balance ||=
        order.client.balances.find_by(currency: Order::MARKET_CURRENCY)
    end

    def token_balance
      @_token_balance ||=
        order.client.balances.find_by(currency: Order::TOKEN_CURRENCY)
    end
  end
end
