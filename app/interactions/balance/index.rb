# frozen_string_literal: true

class Balance < ApplicationRecord
  class Index < ActiveInteraction::Base
    private

    def execute
      Balance.order(:currency).map do |balance|
        balance.as_json.tap do |hsh|
          hsh['available'] = balance.available
        end
      end.as_json
    end
  end
end
