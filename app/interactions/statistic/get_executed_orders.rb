# frozen_string_literal: true

module Statistic
  class GetExecutedOrders < ActiveInteraction::Base
    private

    date   :date
    string :pair, default: 'usdbtc'

    def execute
      return [:not_found, nil] unless data

      prices = []

      result = data.each_with_index.map do |row, i|
        time, market_price, = row
        time = Time.zone.parse time

        next if market_price == '—'
        market_price = market_price.to_d
        cancelled_order = cancelled_orders_by_time[time]
        exec_order      = executed_orders_by_time[time]
        order           = orders_by_time[time]

        prices << market_price if (i % 1).zero?

        {
          time:                 time.to_time.strftime('%H:%M'),
          price:                market_price.to_f,
          exec_order.try(:side) => exec_order.try(:unit_price),
          bid:                  (order.try(:unit_price) if order.try(:side) == 'buy'),
          ask:                  (order.try(:unit_price) if order.try(:side) == 'sell'),
          cancell_bid:          (cancelled_order.try(:unit_price) if cancelled_order.try(:side) == 'buy'),
          cancell_ask:          (cancelled_order.try(:unit_price) if cancelled_order.try(:side) == 'sell'),
          short_period_ema:     prices.last(30).exponential_moving_average.round(2),
          long_period_ema:      prices.last(60).exponential_moving_average.round(2)
        }
      end

      [:ok, result]
    end

    def orders
      @_orders ||= Order.where(created_at: date.all_day)
    end

    def executed_orders_by_time
      @_executed_orders_by_time ||= orders.executed.each_with_object({}) do |o, hsh|
        hsh[o.executed_at] = o
      end
    end

    def cancelled_orders_by_time
      @_cancelled_orders_by_time ||= orders.cancelled.each_with_object({}) do |o, hsh|
        hsh[o.cancelled_at] = o
      end
    end

    def orders_by_time
      @_orders_by_time ||= orders.each_with_object({}) do |o, hsh|
        hsh[o.created_at] = o
      end
    end

    def data
      @_data ||= DATA.dig(pair, date.to_s)
    end
  end
end
