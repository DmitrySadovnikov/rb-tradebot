# frozen_string_literal: true

module Bot
  class Start < ActiveInteraction::Base
    private

    object  :client, class: Client, default: -> { @_client ||= Client.first }
    string  :pair,                  default: 'usdbtc'
    date    :date
    integer :minutes, default: 1440

    def execute
      return :not_found unless data

      data.each_with_index do |row, i|
        shit_time, shit_price, = row
        current_time = Time.zone.parse(shit_time)

        next if shit_price.blank? || shit_price == '—'

        timestamp    = current_time.to_i
        market_price = shit_price.to_d

        market_prices     << market_price if (i % 1).zero?
        short_period_emas << market_prices.last(30).ema.round(2)
        long_period_emas  << market_prices.last(60).ema.round(2)

        Bot::MakeDecision.run!(
          current_time:      current_time,
          market_price:      market_price,
          short_period_emas: short_period_emas,
          long_period_emas:  long_period_emas,
          timestamp:         timestamp,
          client:            client
        )
      end

      :ok
    end

    def short_period_emas
      @_short_period_emas ||= []
    end

    def long_period_emas
      @_long_period_emas ||= []
    end

    def market_prices
      @_market_prices ||= []
    end

    def data
      @_data ||= DATA.dig(pair, date.to_s).try(:first, minutes)
    end
  end
end
