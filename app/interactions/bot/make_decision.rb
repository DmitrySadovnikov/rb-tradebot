# frozen_string_literal: true

module Bot
  class MakeDecision < ActiveInteraction::Base
    private

    STOP_LOSS_RATE = 0.02
    PROFIT_RATE    = 0.002
    TOKEN_MIN_ASK  = 0.001
    MONEY_MIN_BID  = 1.0

    time    :current_time
    integer :timestamp
    decimal :market_price
    object  :client, class: Client

    array(:short_period_emas) { decimal }
    array(:long_period_emas)  { decimal }

    def execute
      if '2017-12-21 02:28:00 UTC'.to_time.to_i <= timestamp
        # require 'pry'; Kernel.binding.pry; # TODO: DELETE IT
      end
      return false if short_period_emas.size < 60

      # require 'pry'; Kernel.binding.pry if lines_crossed?

      fake_execute if active_orders.present?

      cancel_old_orders
      cancel_sell_orders if uptrend?   && (trend_changed? || grows?)
      cancel_buy_orders  if downtrend? && (trend_changed? || !grows?)

      result = case
               when need_to_sell? then make_ask
               when need_to_buy?  then make_bid
               end

      if result.present?
        client.new_order(*prepare_result(**result))
      else
        [:do_nothing, nil]
      end
    end

    def need_to_sell?
      downtrend? && have_enough_tokens? && (good_price_to_sell? || dump?)
    end

    def need_to_buy?
      uptrend? && have_enough_money? && grows? && (good_price_to_buy? || pump?)
    end

    def good_price_to_sell?
      last_sell_price < my_sell_price
    end

    def good_price_to_buy?
      last_buy_amount < bid_token_amount
    end

    def last_executed_order
      client.orders.executed.last
    end

    def trend_changed?
      sign = uptrend? ? '<' : '>'

      (-3..-2).to_a.map do |i|
        short_period_emas[i].send(sign, long_period_emas[i])
      end.inject(:&)
    end

    def make_ask
      {
        symbol:          'usdbtc',
        original_amount: available_tokens,
        type:            'market',
        side:            'sell',
        price:           ask_price,
        timestamp:       timestamp,
        created_at:      current_time,
        unit_price:      my_sell_price
      }
    end

    def make_bid
      {
        symbol:          'usdbtc',
        original_amount: bid_token_amount,
        type:            'market',
        side:            'buy',
        price:           available_money,
        timestamp:       timestamp,
        created_at:      current_time,
        unit_price:      my_buy_price
      }
    end

    def ask_price
      (available_tokens * my_sell_price).round(8)
    end

    def bid_token_amount
      (available_money / my_buy_price).round(8)
    end

    def lines_crossed?
      (100 - (last_long_period_ema / last_short_period_ema * 100)).abs <= 0.04
    end

    def lines_uncrossed?
      (100 - (last_long_period_ema / last_short_period_ema * 100)).abs > 0.04
    end

    def downtrend?
      long_period_emas.last > short_period_emas.last
    end

    def uptrend?
      long_period_emas.last < short_period_emas.last
    end

    def last_short_period_ema
      @_last_short_period_ema ||= short_period_emas.last
    end

    def last_long_period_ema
      @_last_long_period_ema ||= long_period_emas.last
    end

    def have_enough_tokens?
      available_tokens >= TOKEN_MIN_ASK
    end

    def have_enough_money?
      available_money >= MONEY_MIN_BID
    end

    def prepare_result(symbol:, original_amount:, type:, side:, price:, timestamp:, created_at:, unit_price:)
      [
        symbol,
        original_amount,
        type,
        side,
        price,
        {
          timestamp:  timestamp,
          created_at: created_at,
          unit_price: unit_price
        }
      ]
    end

    def cancel_old_orders
      # если тренд идет вверх, а ордера стоят на продажу
      # если тренд идет вниз, а ордера на покупку
      # если если цена ордера существенно отличаеися от цены рынка
      # если ордер старый


      sec = 300
      old_active_orders = active_orders.where('(timestamp + ?) <= ?', sec, timestamp)
      ids               = old_active_orders.map(&:id)
      client.cancel_orders(ids, cancelled_at: current_time) if ids.present?
    end

    def cancel_sell_orders
      ids = active_orders.where(side: 'sell').pluck(&:id)
      client.cancel_orders(ids, cancelled_at: current_time) if ids.present?
    end

    def cancel_buy_orders
      ids = active_orders.where(side: 'buy').pluck(&:id)
      client.cancel_orders(ids, cancelled_at: current_time) if ids.present?
    end

    def active_orders
      client.orders.active
    end

    def fake_execute
      sell = active_orders.where(side: 'sell')
                          .where('unit_price <= ?', market_price)
      buy  = active_orders.where(side: 'buy')
                          .where('unit_price >= ?', market_price)

      sell.or(buy).each do |o|
        o.execute(o.original_amount, executed_at: current_time)
      end
    end

    def available_tokens
      token_balance.reload.available
    end

    def available_money
      money_balance.reload.available
    end

    def money_balance
      @_money_balance ||=
        client.balances.find_by(currency: Order::MARKET_CURRENCY)
    end

    def token_balance
      @_token_balance ||=
        client.balances.find_by(currency: Order::TOKEN_CURRENCY)
    end

    def my_sell_price
      last_short_period_ema * (1 - PROFIT_RATE)
    end

    def my_buy_price
      last_short_period_ema * (1 + PROFIT_RATE)
    end

    # def very_fast_grows?
    #   (100 - (short_period_emas[-11] / last_short_period_ema * 100)).abs > 0.08
    # end

    def grows?
      (-3..-2).to_a.map do |i|
        short_period_emas[i] < last_short_period_ema &&
          long_period_emas[i] < last_long_period_ema
      end.inject(:&)
    end

    def dump?
      last_sell_price * (1 - STOP_LOSS_RATE) > last_short_period_ema
    end

    def pump?
      last_sell_price * (1 + STOP_LOSS_RATE) < last_short_period_ema
    end

    def last_sell_price
      last_executed_order.price / last_executed_order.original_amount
    end

    def last_buy_amount
      last_executed_order.try(:original_amount) || 0
    end
  end
end
