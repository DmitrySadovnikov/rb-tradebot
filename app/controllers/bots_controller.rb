# frozen_string_literal: true

class BotsController < ApplicationController
  def start
    Order.destroy_all
    Balance.reset
    result = Bot::Start.run!(params)

    # result = (1..6).to_a.map do |i|
    #   Bot::Start.run!(date: "2017-12-0#{i}")
    # end.first
    render json: { status: result }
  end
end
