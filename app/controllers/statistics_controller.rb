# frozen_string_literal: true

class StatisticsController < ApplicationController
  # http://localhost:3000/?date=2017-12-13&pair=usdbtc&minutes=1440
  def orders_by_time
    status, result = Statistic::GetExecutedOrders.run!(params)
    render json: { collection: result, status: status }
  end
end
