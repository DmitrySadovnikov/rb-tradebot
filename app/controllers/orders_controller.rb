# frozen_string_literal: true

class OrdersController < ApplicationController

  def index
    result = Order::Index.run!(date: params[:date])
    render json: { collection: result, status: :ok }
  end

  def destroy_all
    Order.destroy_all
    render json: { status: :ok }
  end
end
