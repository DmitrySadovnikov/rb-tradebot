# frozen_string_literal: true

class BalancesController < ApplicationController
  def index
    render json: { collection: Balance::Index.run! }
  end

  def update
    balance = Balance.find(params[:id])
    balance.update!(params)

    render json: { resource: balance }
  end

  def reset
    Balance.reset
    render json: { resource: :ok }
  end
end
