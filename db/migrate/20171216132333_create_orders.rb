class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.references :client

      t.string  :symbol
      t.string  :side
      t.integer :timestamp

      t.decimal :price
      t.decimal :original_amount
      t.decimal :executed_amount
    end
  end
end
