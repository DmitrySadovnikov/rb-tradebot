class CreateBalances < ActiveRecord::Migration[5.1]
  def change
    create_table :balances do |t|
      t.references :client

      t.string   :currency
      t.decimal  :amount
    end

    add_index :balances, %i[client_id currency], unique: true
  end
end
