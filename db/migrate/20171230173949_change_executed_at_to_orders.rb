class ChangeExecutedAtToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :created_at, :datetime
    add_column :orders, :executed_at, :datetime
    remove_column :orders, :date, :date
    remove_column :orders, :executed_timestamp, :integer
  end
end
