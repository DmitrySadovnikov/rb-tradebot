class AddTestDateToOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :test_date, :datetime
  end
end
