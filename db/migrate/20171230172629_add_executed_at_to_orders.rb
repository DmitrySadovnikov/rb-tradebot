class AddExecutedAtToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :executed_timestamp, :integer
  end
end
