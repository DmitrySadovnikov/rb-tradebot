# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180101180833) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "balances", force: :cascade do |t|
    t.bigint "client_id"
    t.string "currency"
    t.decimal "amount"
    t.index ["client_id", "currency"], name: "index_balances_on_client_id_and_currency", unique: true
    t.index ["client_id"], name: "index_balances_on_client_id"
  end

  create_table "clients", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "client_id"
    t.string "symbol"
    t.string "side"
    t.decimal "price"
    t.decimal "original_amount"
    t.decimal "executed_amount"
    t.integer "status", default: 0, null: false
    t.decimal "unit_price"
    t.integer "timestamp"
    t.datetime "created_at"
    t.datetime "executed_at"
    t.datetime "cancelled_at"
    t.index ["client_id"], name: "index_orders_on_client_id"
  end

end
